window.onload = function() {

  var eventSource = new EventSource("/eventsource");
  
  eventSource.addEventListener('testsuitechange', function(e) {
    var el = document.getElementById("tests-script");
    if (el) {
      el.parentElement.removeChild(el);
    }
    el = document.createElement("script");
    el.id = "tests-script";
    el.type = "text/javascript";
    el.src = e.data + "?nocache=" + new Date().getTime();
    document.body.appendChild(el);

    //var el2 = document.getElementById("run").submit()
    
    setTimeout(function(){qx.core.Init.getApplication().runner.runTests()}, 1000);
    
  }, false);
  
};